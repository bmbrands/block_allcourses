<?php

/**
 * Displays a course listing on the users mymoodle page (if added by admin)
 *
 * @copyright  2012 Sonsbeekmedia.nl
 * @author     Bas Brands <bmbrands@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */



defined('MOODLE_INTERNAL') || die();

class block_allcourses_edit_form extends block_edit_form {
    protected function specific_definition($mform) {
        global $CFG;
        $mform->addElement('header', 'configheader', get_string('allcourses_settings', 'block_allcourses'));

       
    }
}
