<?php
/**
 * Displays a course listing on the users mymoodle page (if added by admin)
 *
 * @copyright  2012 Sonsbeekmedia.nl
 * @author     Bas Brands <bmbrands@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_login();

$PAGE->set_url('/blocks/allcourses/allcoursespage.php');
$systemcontext = get_context_instance(CONTEXT_SYSTEM);
$PAGE->set_context($systemcontext);
$PAGE->set_pagelayout('allcourses');

echo $OUTPUT->header();

$allcourses_out = allcourses_get_courses();

echo $allcourses_out;

echo $OUTPUT->footer();

function allcourses_get_courses() {
	global $CFG, $USER, $DB, $OUTPUT, $PAGE;

	$allcourses_out = '';

	require_once $CFG->dirroot.'/course/lib.php' ;
	$courses    = get_courses_wmanagers('all','c.sortorder ASC',array('summary','summaryformat'));

	if ($courses) {
		$allcourses_out .=  html_writer::start_tag('ul', array('class'=>'unlist'));
		$enrolledlist = enrol_get_my_courses('id');

		foreach ($courses as $course) {

			$coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);
			if ($course->visible == 1 || has_capability('moodle/course:viewhiddencourses', $coursecontext)) {
				$allcourses_out .=  html_writer::start_tag('li');
				$allcourses_out .=  allcourses_get_course($course);
				$allcourses_out .=  html_writer::end_tag('li');
			}
		}
		$allcourses_out .=  html_writer::end_tag('ul');
	}

	return $allcourses_out;
}

function allcourses_get_course($course) {
	global $CFG, $USER, $DB, $OUTPUT;


	$context = get_context_instance(CONTEXT_COURSE, $course->id);

	// Rewrite file URLs so that they are correct
	$course->summary = file_rewrite_pluginfile_urls($course->summary, 'pluginfile.php', $context->id, 'course', 'summary', NULL);
	
	

	$allcourses_out =html_writer::start_tag('div', array('class'=>'coursebox clearfix courseboxnr'.$course->id));
	$allcourses_out .=html_writer::start_tag('div', array('class'=>'info'));
	$allcourses_out .= html_writer::start_tag('div', array('class'=>'mycoursename'));

	$linkhref = new moodle_url('/course/view.php', array('id'=>$course->id));
	$linktext = highlight('', format_string($course->fullname));
	$linkparams = array('title'=>get_string('entercourse'));
	if (empty($course->visible)) {
		$linkparams['class'] = 'dimmed';
	}
	$allcourses_out .=html_writer::link($linkhref, $linktext, $linkparams);
	$allcourses_out .=html_writer::end_tag('div');

	/// first find all roles that are supposed to be displayed
	if (!empty($CFG->coursecontact)) {
		$managerroles = explode(',', $CFG->coursecontact);
		$namesarray = array();
		if (isset($course->managers)) {
			if (count($course->managers)) {
				$rusers = $course->managers;
				$canviewfullnames = has_capability('moodle/site:viewfullnames', $context);

				/// Rename some of the role names if needed
				if (isset($context)) {
					$aliasnames = $DB->get_records('role_names', array('contextid'=>$context->id), '', 'roleid,contextid,name');
				}

				// keep a note of users displayed to eliminate duplicates
				$usersshown = array();
				foreach ($rusers as $ra) {

					// if we've already displayed user don't again
					if (in_array($ra->user->id,$usersshown)) {
						continue;
					}
					$usersshown[] = $ra->user->id;

					$fullname = fullname($ra->user, $canviewfullnames);

					if (isset($aliasnames[$ra->roleid])) {
						$ra->rolename = $aliasnames[$ra->roleid]->name;
					}

					$namesarray[] = format_string($ra->rolename).': '.
					html_writer::link(new moodle_url('/user/view.php', array('id'=>$ra->user->id, 'course'=>SITEID)), $fullname);
				}
			}
		} else {
			$rusers = get_role_users($managerroles, $context,
			true, '', 'r.sortorder ASC, u.lastname ASC');
			if (is_array($rusers) && count($rusers)) {
				$canviewfullnames = has_capability('moodle/site:viewfullnames', $context);

				/// Rename some of the role names if needed
				if (isset($context)) {
					$aliasnames = $DB->get_records('role_names', array('contextid'=>$context->id), '', 'roleid,contextid,name');
				}

				foreach ($rusers as $teacher) {
					$fullname = fullname($teacher, $canviewfullnames);

					/// Apply role names
					if (isset($aliasnames[$teacher->roleid])) {
						$teacher->rolename = $aliasnames[$teacher->roleid]->name;
					}

					$namesarray[] = html_writer::link(new moodle_url('/user/view.php', array('id'=>$teacher->id, 'course'=>SITEID)), $fullname);
				}
			}
		}


	}
	$allcourses_out .=html_writer::end_tag('div'); // End of info div
	$allcourses_out .= '<div style="clear:both;">';

	$courseimage = allcourses_get_courseimage($course);
	
	$allcourses_out .=html_writer::start_tag('div', array('class'=>'swetscourse','style'=>'background-image: url('.$courseimage.')'));
	//$allcourses_out .=html_writer::start_tag('div', array('class'=>'swetscourse'));
	$allcourses_out .=html_writer::start_tag('div', array('class'=>'summary'));



	$options = NULL;
	$options->noclean = true;
	$options->para = false;
	$options->overflowdiv = true;
	if (!isset($course->summaryformat)) {
		$course->summaryformat = FORMAT_MOODLE;
	}


	$allcourses_out .=html_writer::end_tag('div'); // End of summary div

	$allcourses_out .=html_writer::start_tag('div', array('class'=>'entercourse'));
	$allcourses_out .=html_writer::link($linkhref, 'enter course', $linkparams);
	$allcourses_out .=html_writer::end_tag('div');
	$allcourses_out .=html_writer::end_tag('div'); // End of swetscourse div


	$allcourses_out .=html_writer::start_tag('div', array('class'=>'swetsteachers'));
	if (!empty($namesarray)) {
		$allcourses_out .=html_writer::start_tag('ul', array('class'=>'teachers'));
		foreach ($namesarray as $name) {
			$allcourses_out .=html_writer::tag('li', $name);
		}
		$allcourses_out .=html_writer::end_tag('ul');
	}
	$allcourses_out .=html_writer::end_tag('div');
	$allcourses_out .=html_writer::end_tag('div'); // End of coursebox div

	return $allcourses_out;
}

function allcourses_get_courseimage($course) {
	global $CFG;
	$context = get_context_instance(CONTEXT_COURSE, $course->id);
	$component = 'backup';
	$filearea  = 'course';
	$itemid    = 0;
	list($context, $course, $cm) = get_context_info_array($context->id);
	$fs = get_file_storage();
	$dir = $fs->get_area_tree($context->id, $component, $filearea, $itemid);

	foreach ($dir['files'] as $file) {
		$url = file_encode_url("$CFG->wwwroot/pluginfile.php", '/'.$context->id.'/'.$component.'/'.$filearea.'/'.$file->get_filepath().$file->get_filename(), false);
		return $url;
	}
}
?>
