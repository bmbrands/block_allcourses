<?php

/**
 * Displays a course listing on the users mymoodle page (if added by admin)
 *
 * @copyright  2012 Sonsbeekmedia.nl
 * @author     Bas Brands <bmbrands@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$string['pluginname'] = 'All Courses';
$string['myprofile_settings'] = 'All Courses settings';
$string['contentsettings'] = 'Nothing here yet';
